<?php
//session_start();
//if ($_SESSION['validUser'] == "yes")	//If this is a valid user allow access to this page
$password="";
$message="Password Incorrect, Please Try Again";
	
	include 'connection.php';
	if(isset($_POST["submit"]))
	{	
		//The form has been submitted and needs to be processed
		
		//Get the name value pairs from the $_POST variable into PHP variables
		//The example uses variables with the same name as the name atribute from the form
		$student_firstName = $_POST["student_firstName"];
		$student_lastName = $_POST["student_lastName"];
		$student_emailAddress = $_POST["student_emailAddress"];
		$student_hometown = $_POST["student_hometown"];
		$student_programStudy = $_POST["student_programStudy"];
		$student_personalWebsite = $_POST["student_personalWebsite"];
		$student_careerGoals = $_POST["student_careerGoals"];
		$student_threeWords = $_POST["student_threeWords"];
		$student_mainImage = $_POST["student_mainImage"];
		$student_popupImage = $_POST["student_popupImage"];
		$student_ID = $_POST["student_ID"];	//from the hidden field of the update form
		
		//Create the SQL UPDATE query or command  
		$sql = "UPDATE student_db SET " ;
		$sql .= "student_firstName=?, ";
		$sql .= "student_lastName=?, ";
		$sql .= "student_emailAddress=?, ";
		$sql .= "student_hometown=?, ";
		$sql .= "student_programStudy=?, ";	
		$sql .= "student_personalWebsite=?, ";
		$sql .= "student_careerGoals=?, ";
		$sql .= "student_threeWords=?, ";
		$sql .= "student_mainImage=?, ";
		$sql .= "student_popupImage=? ";	//NOTE last one does NOT have a comma after it
		$sql .= " WHERE (student_ID='$student_ID')"; //VERY IMPORTANT  
		
		//echo "<h3>$sql</h3>";			//testing
	
		$query = $conn->prepare($sql);	//Prepare SQL query
	
		$query->bind_param('ssssssssss',$student_firstName,$student_lastName,$student_emailAddress,$student_hometown,$student_programStudy,$student_personalWebsite,$student_careerGoals,$student_threeWords,$student_mainImage,$student_popupImage);
	
		if ( $query->execute() )
		{
			$message = "<h1>Your record has been successfully UPDATED the database.</h1>";
			//$message .= "<p>Please <a href='studentList.php'>view</a> your records.</p>";
		}
		else
		{
			$message = "<h1>You have encountered a problem.</h1>";
			$message .= "<h2 style='color:red'>" . mysqli_error($conn) . "</h2>";
		}
				
	}//end if submitted
	else	
	{
		//The form needs to display the fields of the record to the user for changes
		$updateStudentID = $_GET['recId'];	//Record Id to be updated
		//$updateRecId = 2;				//Hard code a key for testing purposes
		
		//echo "<h1>updateStudentId: $updateStudentID</h1>";
		
		//Finds a specific record in the table
		$sql = "SELECT student_ID,student_firstName,student_lastName,student_emailAddress,student_hometown,student_programStudy,student_personalWebsite,student_careerGoals,student_threeWords,student_mainImage,student_popupImage FROM student_db WHERE student_ID=?";	
		//echo "<p>The SQL Command: $sql </p>"; //For testing purposes as needed.
	
		$query = $conn->prepare($sql);
		
		$query->bind_param("i",$updateStudentID);	
	
		if( $query->execute() )	//Run Query and Make sure the Query ran correctly
		{
			$query->bind_result($student_ID,$student_firstName,$student_lastName,$student_emailAddress,$student_hometown,$student_programStudy,$student_personalWebsite,$student_careerGoals,$student_threeWords,$student_mainImage,$student_popupImage);
		
			$query->store_result();
			
			$query->fetch();
		}
		else
		{
			$message = "<h1>You have encountered a problem with your update.</h1>";
			$message .= "<h2>" . mysqli_error($conn) . "</h2>" ;			
		}
	
	}//end else submitted
//}//end Valid User True
//else
//{
	//Invalid User attempting to access this page. Send person to Login Page
//	header('Location: presentersLogin.php');
//}	
?>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Student Form</title>

<link rel="stylesheet" type="text/css" href="studentForm.css">
</head>
<body>
  <div>
  <img src="images/logo.png" height="200px" width="200px" class="center">

<?php
//If the user submitted the form the changes have been made
if(isset($_POST["submit"]))
{
	echo $message;	//contains a Success or Failure output content
}//end if submitted

else
{	//The page needs to display the form and associated data to the user for changes
?>
<form id="studentForm" name="studentForm" method="post" action="<?php echo $_SERVER['SCRIPT_NAME'];?>">

    <label for="student_firstName">First Name</label>
    <input type="text" id="student_firstName" name="student_firstName" required="required" value="<?php echo $student_firstName;?>"/>

    <label for="student_lastName">Last Name</label>
    <input type="text" id="student_lastName" name="student_lastName" required="required" value="<?php echo $student_lastName;?>"/>

    <label for="student_emailAddress">Email Address</label>
    <input type="text" id="student_emailAddress" name="student_emailAddress" required="required" value="<?php echo $student_emailAddress;?>"/>

    <label for="student_hometown">Hometown</label>
    <input type="text" id="student_hometown" name="student_hometown" required="required" value="<?php echo $student_hometown;?>"/>

    <label for="student_programStudy">Program of Study</label>
    <!-- <input type="text" id="student_programStudy" name="student_programStudy" required="required" value=" --><!-- <?php echo $student_programStudy;?>"/> -->
    <select name="student_programStudy">
      <option>Select Program of Study</option>
      <option value="Animation"<?php if ($student_programStudy=="Animation") echo 'selected="selected"';?>>Animation</option>
      <option value="Graphic Design"<?php if ($student_programStudy=="Graphic Design") echo 'selected="selected"';?>>Graphic Design</option>
      <option value="Photography"<?php if ($student_programStudy=="Photography") echo 'selected="selected"';?>>Photography</option>
      <option value="Video Production"<?php if ($student_programStudy=="Video Production") echo 'selected="selected"';?>>Video Production</option>
      <option value="Web Development"<?php if ($student_programStudy=="Web Development") echo 'selected="selected"';?>>Web Development</option>
    </select>

    <label for="student_personalWebsite">Website</label>
    <input type="text" id="student_personalWebsite" name="student_personalWebsite" required="required" value="<?php echo $student_personalWebsite;?>"/>

    <label for="student_careerGoals">Future Career Goals</label>
    <textarea id="student_careerGoals" name="student_careerGoals" rows="5" required="required"><?php echo $student_careerGoals;?></textarea>

    <label for="student_threeWords">Three Words to Describe Yourself</label>
    <textarea id="student_threeWords" name="student_threeWords" rows="3" required="required"><?php echo $student_threeWords;?></textarea>

    <label for="student_mainImage">Main Image</label>
    <input type="text" id="student_mainImage" name="student_mainImage" required="required" value="<?php echo $student_mainImage;?>"/>

    <label for="student_popupImage">Bio Image</label>
    <input type="text" id="student_popupImage" name="student_popupImage" required="required" value="<?php echo $student_popupImage;?>"/>

  	<input type="hidden" name="student_ID" id="student_ID"
    	value="<?php echo $student_ID ?>" />
  
  <p>
    <input type="submit" name="submit" id="submit" value="Update" />
    <input type="reset" name="button2" id="button2" value="Clear Form" />
  </p>
</form>

<?php
}//end else submitted
$query->close();
$conn->close();
?>
<p>Return to <a href="studentList.php">student list.</a></p>
</body>
</html>