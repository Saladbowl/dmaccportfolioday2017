<?php 
    $student_firstName = "";
    $student_lastName = "";
    $student_emailAddress = "";   
    $student_hometown = "";
    $student_programStudy = "";
    $student_personalWebsite = "";
    $student_careerGoals = "";
    $student_threeWords = "";
    //$student_mainImage = "";
    //$student_popupImage = "";
    $password = "";
    $message = "PASSWORD INCORRECT - PLEASE TRY AGAIN";
    $message2 = "";
    $form = "";

if  (isset($_POST["submit"]))
    {	
    	include 'connection.php';

    	$student_firstName = $_POST['student_firstName'];
      $student_lastName = $_POST['student_lastName'];
      $student_emailAddress = $_POST['student_emailAddress'];      
      $student_hometown = $_POST['student_hometown'];
      $student_programStudy = $_POST['student_programStudy'];
      $student_personalWebsite = $_POST['student_personalWebsite'];
      $student_careerGoals = $_POST['student_careerGoals'];
      $student_threeWords = $_POST['student_threeWords'];
      //$student_mainImage = $_POST['student_mainImage'];
      //$student_popupImage = $_POST['student_popupImage'];
      $password = $_POST['password'];

      if ($password == "reveal")
      {
        
      $sqlHardCode = "INSERT INTO student_db (student_firstName, student_lastName, student_emailAddress, student_hometown, student_programStudy, student_personalWebsite, student_careerGoals, student_threeWords) VALUES (?,?,?,?,?,?,?,?);";

        //echo($sqlHardCode);

        // prepare and bind
        $stmt = $conn->prepare($sqlHardCode); //Prepares the query statement 
                
        //Binds the parameters to the query.  
        //The sssii are the data types of the variables in order. 

        $stmt->bind_param("ssssssss", $student_firstName, $student_lastName, $student_emailAddress, $student_hometown, $student_programStudy, $student_personalWebsite, $student_careerGoals, $student_threeWords);

        //Run the SQL prepared statements

        if  ( $stmt->execute()){
            //echo '<script type="text/javascript">alert("execute");</script>';

            $form = "valid";

            $message2 = "<h1>Hooray! Your record has been successfully added to the database.</h1>";
            $message2 .= "<h2>Visit <a style= 'color:white' href='http://dmaccportfolioday.com/'>DMACC</a> Portfolio Day Site.</h2>";
            }
        else
            {
            $message2 = "<h1>You have encountered a problem.</h1>";
            $message2 .= "<h2 style='color:red'>" . mysqli_error($conn) . "</h2>"; //remove this for production purposes
            }

        $stmt->close();
        $conn->close(); 
        }
      } //end if password = reveal
        ?>  


<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Student Form</title>

<link rel="stylesheet" type="text/css" href="studentForm.css">
</head>
<body>
  <div>
  <img src="images/logo.png" height="200px" width="200px" class="center">

  <?php
  if($form == "valid")
    { 
    ?>
     <h3><?php echo($message2); ?></h3>
    <?php
    }//end if
    else{
    ?>

  <h2>Please enter the information you would like to display on the DMACC Portfolio Day Website</h2>

  <form id="studentForm" name="studentForm" method="post" action="<?php echo $_SERVER['SCRIPT_NAME'];?>">

    <label for="student_firstName">First Name</label>
    <input type="text" id="student_firstName" name="student_firstName" required="required" value="<?php echo $student_firstName;?>"/>

    <label for="student_lastName">Last Name</label>
    <input type="text" id="student_lastName" name="student_lastName" required="required" value="<?php echo $student_lastName;?>"/>

    <label for="student_emailAddress">Email Address</label>
    <input type="text" id="student_emailAddress" name="student_emailAddress" required="required" value="<?php echo $student_emailAddress;?>"/>

    <label for="student_hometown">Hometown</label>
    <input type="text" id="student_hometown" name="student_hometown" required="required" value="<?php echo $student_hometown;?>"/>

    <label for="student_programStudy">Program of Study</label>
    <!-- <input type="text" id="student_programStudy" name="student_programStudy" required="required" value=" --><!-- <?php echo $student_programStudy;?>"/> -->
    <select name="student_programStudy">
      <option>Select Program of Study</option>
      <option value="Animation">Animation</option>
      <option value="Graphic Design">Graphic Design</option>
      <option value="Photography">Photography</option>
      <option value="Video Production">Video Production</option>
      <option value="Web Development">Web Development</option>
    </select>

    <label for="student_personalWebsite">Website</label>
    <input type="text" id="student_personalWebsite" name="student_personalWebsite" required="required" value="<?php echo $student_personalWebsite;?>"/>

    <label for="student_careerGoals">Future Career Goals</label>
    <textarea id="student_careerGoals" name="student_careerGoals" rows="5" required="required"><?php echo $student_careerGoals;?></textarea>

    <label for="student_threeWords">Three Words to Describe Yourself</label>
    <textarea id="student_threeWords" name="student_threeWords" rows="3" required="required"><?php echo $student_threeWords;?></textarea>

    <?php
    if  (isset($_POST["submit"]))
    { 
      if ($password != "reveal")
        {
          ?><h3><?php echo($message); ?></h3><?php
        }
    }
    ?>

    <label for="password">Password</label>
    <input type="password" id="password" name="password" required="required"/>
  
    <input type="submit" name="submit" id="submit" value="Submit Info">
    <input type="reset" id="reset" value="Clear Form" >
  </form>

  <?php
    }
    ?>

</div>
</body>
</html>